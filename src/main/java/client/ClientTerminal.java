package client;
/*
 * This class is Client Class.
 *
 * author: Mojtaba Azhdari
 */

import entity.*;
import utility.LoggingUtility;
import utility.XmlUtility;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClientTerminal {

    // **************************************************
    // Fields
    // **************************************************
    private Socket socket;

    // **************************************************
    // Constructor
    // **************************************************
    public ClientTerminal(String serverIpAddress, int serverPort) {
        try {
            XmlUtility.setServerInfo(serverIpAddress, serverPort);
            socket = new Socket(serverIpAddress, serverPort);
        } catch (Exception e) {
            System.out.println("PLEASE FIRST RUN SERVER, THEN TRY TO CONNECT :)");
            System.exit(1);
        }
    }

    public void connectToServer() {
        try {
            LoggingUtility.writeLog("Connecting to BankServer ...");
            List<TransactionResponse> responseList = handleTransactions();
            XmlUtility.saveToXML(responseList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<TransactionResponse> handleTransactions() throws Exception {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        List<TransactionResponse> responseList = new ArrayList<>();
        Map<String, Transaction> transactionMap = XmlUtility.parseTransactions();
        for (Transaction transaction : transactionMap.values()) {
            objectOutputStream.writeObject(transaction);
            String transactionResult = dataInputStream.readUTF();
            TransactionResponse transactionResponse = (TransactionResponse) objectInputStream.readObject();
            responseList.add(transactionResponse);
            LoggingUtility.writeLog(transactionResult);
            Thread.sleep(1000);
        }
        LoggingUtility.writeLog("Terminal is disconnected.");
        objectInputStream.close();
        objectOutputStream.close();
        return responseList;
    }
}
