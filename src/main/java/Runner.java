/*
 * This is the main class for Running Client application.
 *
 * author: Mojtaba Azhdari
 */

import client.ClientTerminal;

public class Runner {

    // **************************************************
    // Fields
    // **************************************************
    private static final String _SERVER_IP_ADDRESS = "127.0.0.1";
    private static final int _SERVER_PORT_NUMBER = 9595;

    public static void main(String[] args) {
        ClientTerminal terminal = new ClientTerminal(_SERVER_IP_ADDRESS, _SERVER_PORT_NUMBER);
        terminal.connectToServer();
    }
}
