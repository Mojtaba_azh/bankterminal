package entity;

public enum TransactionType {
    DEPOSIT,
    WITHDRAW
}
