package entity;
/*
 * This class is used for defining Transactions.
 *
 * author: Mojtaba Azhdari
 */

import java.io.Serializable;

public class Transaction implements Serializable {
    private static final long serialVersionUID = 6529685098267757690L;

    // **************************************************
    // Fields
    // **************************************************
    private String transactionId;
    private TransactionType transactionType;
    private String amount;
    private String depositId;

    public Transaction(String transactionId, TransactionType transactionType, String amount, String depositId) {
        this.transactionId = transactionId;
        this.transactionType = transactionType;
        this.amount = amount;
        this.depositId = depositId;
    }
}
