package entity;
/*
 * This class is used for defining Responses of Transactions.
 *
 * author: Mojtaba Azhdari
 */

import java.io.Serializable;

public class TransactionResponse implements Serializable {
    private static final long serialVersionUID = 7232659830023086970L;

    // **************************************************
    // Fields
    // **************************************************
    private String transactionId;
    private String depositId;
    private boolean result;
    private String remainingBalance;
    private String description;

    // **************************************************
    // Constructor
    // **************************************************
    public TransactionResponse(boolean result, String transactionId, String depositId, String description, String remainingBalance) {
        this.result = result;
        this.transactionId = transactionId;
        this.depositId = depositId;
        this.description = description;
        this.remainingBalance = remainingBalance;
    }

    // **************************************************
    // Getter methods
    // **************************************************
    public String getTransactionId() {
        return transactionId;
    }

    public String getDepositId() {
        return depositId;
    }

    public boolean getResult() {
        return result;
    }

    public String getRemainingBalance() {
        return remainingBalance;
    }

    public String getDescription() {
        return description;
    }

}
