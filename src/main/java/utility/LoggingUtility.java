package utility;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public abstract class LoggingUtility {
    private static Logger serverLogger;
    private static final String _SERVER_LOG_FILE = "log-file/term21374.log";
    private static final Level _LEVEL = Level.INFO;

    static {
        serverLogger = Logger.getLogger("terminalLogger");
        FileHandler fileHandler = null;
        try {
            fileHandler = new FileHandler(_SERVER_LOG_FILE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert fileHandler != null;
        serverLogger.addHandler(fileHandler);
        SimpleFormatter simpleFormatter = new SimpleFormatter();
        fileHandler.setFormatter(simpleFormatter);
    }

    public static void writeLog(String log) {
        serverLogger.log(_LEVEL, log);
    }
}
