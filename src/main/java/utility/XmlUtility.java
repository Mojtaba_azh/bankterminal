package utility;
/*
 * This class is used for working with xml files (Singleton Pattern)
 *
 * author Mojtaba Azhdari
 */

import entity.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class XmlUtility {

    private static final String _TERMINAL_CONFIG_FILE = "xml-files/terminal.xml";
    private static final String _TRANSACTION_RESPONSE_FILE = "xml-files/response.xml";

    public static void setServerInfo(String serverIPAddress, int serverPortNumber) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        File xmlfile = new File(_TERMINAL_CONFIG_FILE);
        Document document = documentBuilder.parse(xmlfile);
        document.getDocumentElement().normalize();
        Node serverNode = document.getElementsByTagName("server").item(0);
        NamedNodeMap serverNodeAttributes = serverNode.getAttributes();
        Node ipAttribute = serverNodeAttributes.getNamedItem("ip");
        Node portAttribute = serverNodeAttributes.getNamedItem("port");
        ipAttribute.setTextContent(serverIPAddress);
        portAttribute.setTextContent(serverPortNumber + "");

        // Write new server ip address and port number to terminal.xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult consoleResult = new StreamResult(xmlfile);
        transformer.transform(source, consoleResult);

    }

    public static Map<String, Transaction> parseTransactions() throws Exception {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        File xmlFile = new File(_TERMINAL_CONFIG_FILE);
        Document document = documentBuilder.parse(xmlFile);
        document.getDocumentElement().normalize();
        NodeList nodeList = document.getElementsByTagName("transaction");

        Map<String, Transaction> transactionMap = new HashMap<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node transactionNode = nodeList.item(i);
            try {
                NamedNodeMap transactionAttributes = transactionNode.getAttributes();
                String transactionId = transactionAttributes.getNamedItem("id").getNodeValue();
                TransactionType transactionType = TransactionType.valueOf(transactionAttributes.getNamedItem("type").getNodeValue().toUpperCase());
                String amount = transactionAttributes.getNamedItem("amount").getNodeValue();
                String depositId = transactionAttributes.getNamedItem("deposit").getNodeValue();

                Transaction transaction = new Transaction(transactionId, transactionType, amount, depositId);
                transactionMap.put(transactionId, transaction);
            } catch (IllegalArgumentException e) {
                LoggingUtility.writeLog("ERROR-4 * WRONG TRANSACTION_TYPE   :  The value of type in your terminal.xml file, should be \'deposit\' or \'withdraw\' !");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return transactionMap;
    }

    public static void saveToXML(List<TransactionResponse> responseList) throws TransformerException, ParserConfigurationException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        Element transactionsElement = document.createElement("transactions");
        document.appendChild(transactionsElement);

        for (TransactionResponse response : responseList) {
            Element transactionElement = document.createElement("transaction");
            transactionsElement.appendChild(transactionElement);

            transactionElement.setAttribute("transactionId", response.getTransactionId());
            transactionElement.setAttribute("depositId", response.getDepositId());
            transactionElement.setAttribute("result", response.getResult() + "");
            transactionElement.setAttribute("remaining-balance", response.getRemainingBalance());
            transactionElement.setAttribute("description", response.getDescription());
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(_TRANSACTION_RESPONSE_FILE));
        transformer.transform(source, result);
    }

}
